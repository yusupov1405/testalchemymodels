from sqlalchemy.orm import Session

import models
import schemas


def create_group(db: Session, group: schemas.GroupCreate) -> models.Groups:
    """Создание группы."""
    db_group = models.Groups(name=group.name, parent_id=group.parent_id)
    db.add(db_group)
    db.commit()
    db.refresh(db_group)
    return db_group


def get_group_by_name(db: Session, name: str):
    """Получение группы по названию."""
    return db.query(models.Groups).filter(models.Groups.name == name)


def get_groups(db: Session):
    """Получение групп."""
    return db.query(models.Groups).all()
