from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session

import crud
import schemas
from database import SESSION_LOCAL

app = FastAPI()


def get_db():
    db = SESSION_LOCAL()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/create_group/", response_model=schemas.Group)
async def create_group(group: schemas.GroupCreate, db: Session = Depends(get_db)):
    return crud.create_group(db=db, group=group)


@app.get("/groups/", response_model=list[schemas.Group])
async def read_groups(db: Session = Depends(get_db)):
    return crud.get_groups(db=db)
