from pydantic import BaseModel


class GroupBase(BaseModel):
    name: str
    parent_id: int | None = None


class GroupCreate(GroupBase):
    ...


class Group(GroupBase):
    id: int
    parent: "Group" = None

    class Config:
        orm_mode = True


